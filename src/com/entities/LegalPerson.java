package com.entities;

public class LegalPerson extends Taxpayer {

    private Integer Employees;

    public LegalPerson() {
        super();
    }

    public LegalPerson(String name, Double annIncome, Integer employees) {
        super(name, annIncome);
        Employees = employees;
    }

    public Integer getEmployees() {
        return Employees;
    }

    public void setEmployees(Integer employees) {
        Employees = employees;
    }

    @Override
    public double tax() {
        if (getEmployees() < 10) {
            return getAnnIncome() * 0.16;
        }
        else {
            return getAnnIncome() * 0.14;
        }
    }
}
