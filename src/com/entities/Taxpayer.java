package com.entities;

public abstract class Taxpayer {

    private String name;
    private Double annIncome;

    public Taxpayer() {
    }

    public Taxpayer(String name, Double annIncome) {
        this.name = name;
        this.annIncome = annIncome;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAnnIncome() {
        return annIncome;
    }

    public void setAnnIncome(Double annIncome) {
        this.annIncome = annIncome;
    }

    public abstract double tax();
}
