package com.entities;

public class NaturalPerson extends Taxpayer {

    private Double healthExpenses;

    public NaturalPerson() {
        super();
    }

    public NaturalPerson(String name, Double annIncome, Double healthExpenses) {
        super(name, annIncome);
        this.healthExpenses = healthExpenses;
    }

    public Double getHealthExpenses() {
        return healthExpenses;
    }

    public void setHealthExpenses(Double healthExpenses) {
        this.healthExpenses = healthExpenses;
    }

    @Override
    public double tax() {
        if (getAnnIncome() < 20000.0) {
            return getAnnIncome() * 0.15 - healthExpenses * 0.5;
        }
        else {
            return getAnnIncome() * 0.25 - healthExpenses * 0.5;
        }
    }
}
