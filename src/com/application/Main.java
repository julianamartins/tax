package com.application;

import com.entities.LegalPerson;
import com.entities.NaturalPerson;
import com.entities.Taxpayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        List<Taxpayer> list = new ArrayList<>();
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the number of tax payers: ");
        int n  = sc.nextInt();
        for (int i=1; i<=n; i++) {
            System.out.println("Tax paper #" + i + " data:");
            System.out.print("Natural Person or Legal Person (n/l)? ");
            char ch = sc.next().charAt(0);
            System.out.print("Name: ");
            sc.nextLine();
            String name = sc.nextLine();
            System.out.print("Annual Income: ");
            double annualIncome = sc.nextDouble();
            if (ch =='n') {
                System.out.print("Health Expenditures: ");
                double healthExpenditures = sc.nextDouble();
                list.add(new NaturalPerson(name, annualIncome, healthExpenditures));
            }
            else {
                System.out.print("Number of Employees: ");
                int numberEmployees = sc.nextInt();
                list.add(new LegalPerson(name, annualIncome, numberEmployees));
            }
        }

        System.out.println();
        double sum = 0.0;
        System.out.println("TAXES PAID");
        for (Taxpayer taxpayer : list) {
            double tax = taxpayer.tax();
            System.out.println(taxpayer.getName()
                    + ": $ "
                    + String.format("%.2f", tax));
            sum += tax;
        }

        System.out.println();
        System.out.println("TOTAL TAXES: $ " + String.format("%.2f", sum));
        sc.close();
    }
}
